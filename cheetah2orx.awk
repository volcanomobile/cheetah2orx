#!/bin/awk -f
{
  if($1 == "textures:")
  {
    fileName = $2;
    print "["fileName"]";
    print "Texture = "fileName;
    printf("\n");
  }
  else
  {
    print "["$1"@"fileName"]";
    print "TextureOrigin = ("$2", "$3", 0)";
    print "TextureSize = ("$4", "$5", 0)";
    printf("\n");
  }
}

